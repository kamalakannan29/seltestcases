package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MergeLeadPage extends ProjectMethods{
	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using="(//img[@alt=\"Lookup\"])[1]") WebElement eleFromLead;
	@FindBy(how = How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.NAME, using="submitButton") WebElement eleCreateLeadSubmit;
	
	public MergeLeadPage enterCompanyName(String companyName) {
		type(eleCompanyName, companyName);
		return this;
	}
	public MergeLeadPage enterFirstName(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}
	public MergeLeadPage enterLastName(String lastName) {
		type(eleLastName, lastName);
		return this;
	}
	public ViewLeadPage clickCreateLeadSubmit() {
		click(eleCreateLeadSubmit);
		return new ViewLeadPage();
	}
}
